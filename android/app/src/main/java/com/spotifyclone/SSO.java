package com.spotifyclone;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

public class SSO extends ReactContextBaseJavaModule {

    private static final int REQUEST_CODE = 1337;
    private static final String REDIRECT_URI = "http://mysite.com/callback/";
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String Response_Error = "Response_Error";
    private static final String Empty_response = "Empty_response";

    public SSO(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    Promise promise;
    WritableMap map;

    @ReactMethod
    protected void authenticate(Promise promise) {

        this.promise = promise;

        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        map = Arguments.createMap();

        AuthenticationRequest.Builder builder =
                new AuthenticationRequest.Builder("4795f46db5b84cd38649de99610ab6a3", AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"streaming","user-follow-read"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(getCurrentActivity(), REQUEST_CODE, request);

    }



    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {

            if (requestCode == REQUEST_CODE) {
                AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);

                switch (response.getType()) {
                    // Response was successful and contains auth token
                    case TOKEN:
                        map.putString("token",response.getAccessToken());
                        // Handle successful response
                        String token = response.getAccessToken();

                        promise.resolve(map);
                        break;

                    // Auth flow returned an error
                    case ERROR:
                        String err = "asdsa";
                        promise.reject(Response_Error,response.getError());
                        break;
                    case EMPTY:
                        // Most likely auth flow was cancelled
                        promise.reject(Empty_response,response.getError());
                    default:
                        promise.reject(Response_Error,response.getError());
                        // Handle other cases
                }
            }
        }
    };







//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }

    @Nonnull
    @Override
    public String getName() {
        return "SpotifySSO";
    }
}
