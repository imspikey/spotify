import React from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";
import styles from "./styles";
import { Rating } from "react-native-ratings";

export const ArtistCard = props => {
  const {
    style,
    image,
    title,
    type,
    followers,
    rating,
    onPress,
    id,
    artistsNames,
    release_date,
    total_tracks,
    previewButton,
  } = props;

  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        disabled={type !== 'artistSearch'}
        onPress={() => onPress(id)}
        style={{ flex: 1 }}
      >
        <Image
          style={styles.artistImage}
          source={{
            uri: image,
          }}
        />
        <View
          style={[
            styles.bottomContainer,
            title.length > 20 ? { flex: 0.5 } : null,
          ]}
        >
          <Text style={styles.title} numberOfLines={2} ellipsizeMode="tail">
            {title}
          </Text>
          <Text style={styles.bottomTitle}>
            {artistsNames ? artistsNames : followers + ', followers'}
          </Text>
          {type === 'artistSearch' ? (
            <Rating
              tintColor={'rgb(39,40,42)'}
              type="star"
              ratingCount={5}
              imageSize={26}
              style={{
                alignItems: "flex-start",
                marginTop: "auto",
                marginBottom: '10%',
                overflow: 'hidden'
              }}
              startingValue={rating}
              readonly={true}
            />
          ) : (
            <Text style={styles.date}>
              {release_date}~{'\n'}
              {total_tracks} tracks
            </Text>
          )}
          {previewButton}
        </View>
      </TouchableOpacity>
    </View>
  );
};
