import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: 290,
    height: 400,
    elevation: 2,
  },
  bottomContainer: {
    paddingLeft: 10,
    paddingRight: 15,
    paddingTop: 10,
    alignItems:'center',
    flex: 0.4,

  },
  artistImage: {
    flex: .7,

  },
  bottomTitle:{
  color:'white',
  },
  date:{
    color:'white',
    fontSize:12
  },
  title: {
    fontSize: 18,
    color:'white',
  }
});

export default styles;
