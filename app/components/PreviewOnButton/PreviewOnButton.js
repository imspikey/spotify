import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
export class PreviewOnButton extends React.Component {
  render() {
    const { albumUrl, text, callback } = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => callback(albumUrl)}
      >
        <Text style={styles.text}>{text}</Text>
      </TouchableOpacity>
    );
  }
}
