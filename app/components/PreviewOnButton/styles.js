import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: '35%',
    backgroundColor: 'rgba(242,242,242,1)',
    marginTop: 'auto'
  },
  text: {
    color: 'gray',
    textAlign: 'center'
  }
});

export default styles;
