export class SpotifyApi {
  static getArtists = (query, limit, offset, token, callback) => {
    fetch(
      `https://api.spotify.com/v1/search?q=${query}&type=artist&limit=${limit}&offset=${offset}`,
      {
        method: 'GET',
        headers: {
          Accept: "application/json",
          'Content-Type': "application/json",
          Authorization: 'Bearer ' + token
        }
      }
    )
      .then(response => {
        return response.json();
      })
      .then(responseJson => {
        callback(responseJson.artists);
      })
      .catch(error => {
        callback(null, error);
      });
  };

  static getArtistAlbums(id, token, limit, offset, callback) {
    fetch(
      `https://api.spotify.com/v1/artists/${id}/albums?limit=${limit}&offset=${offset}`,
      {
        method: 'GET',
        headers: {
          Accept: "application/json",
          'Content-Type': "application/json",
          Authorization: 'Bearer ' + token
        }
      }
    )
      .then(response => {
        return response.json();
      })
      .then(responseJson => {
        callback(responseJson.items);
      })
      .catch(error => {
        callback(null, error);
      });
  }
}
