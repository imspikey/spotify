import React from 'react';
import {
  View,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  Linking,
} from 'react-native';
import { SpotifyApi } from '../../lib/api/SpotifyApi';
import { ArtistCard } from '../../components/ArtistCard/ArtistCard';
import { PreviewOnButton } from '../../components/PreviewOnButton/PreviewOnButton';

export class ArtistScreen extends React.Component {
  constructor(props) {
    super(props);
    this.id = this.props.navigation.getParam('artistId');
    this.token = this.props.navigation.getParam('token');
    this.state = { text: '', data: {}, refreshing: false };
    this.offset = 0;
  }

  componentDidMount() {
    this.getArtistAlbums();
  }

  static navigationOptions = {
    title: 'Artists',
    headerStyle: {
      backgroundColor: 'rgb(39,40,42)'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    },
  };

  getArtistAlbums = () => {
    this.setState({ refreshing: true });

    SpotifyApi.getArtistAlbums(
      this.id,
      this.token,
      10,
      this.offset,
      (res, err) => {
        if (err) {
          alert(err.message);
          this.setState({ refreshing: false });
          return;
        }

        this.data = this.offset > 0 ? this.data.concat(res) : res;
        console.log(res);
        this.setState({ refreshing: false });
      }
    );
  };

  handleRefresh = () => {
    this.offset = 0;
    this.data = [];
    this.getArtistAlbums(this.previousQuery);
    /*loadMoreAlbums is being called automatically after screen gets updated
    for no good reasons so this code is to stop it from running
    since this is not the desired behaviour*/
    this.cancelLoadMoreAlbums = true;
  };

  loadMoreAlbums = () => {
    if (this.cancelLoadMoreAlbums) {
      this.cancelLoadMoreAlbums = false;
      return;
    }
    this.offset += 10;

    this.getArtistAlbums(this.previousQuery);
  };

  onClickPreview = url => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert("This type of Url is not supported! " + this.props.url);
      }
    });
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: 'rgb(18,18,18)',
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingTop: 30,
        }}
      >
        <FlatList
          contentContainerStyle={{ alignItems: 'center' }}
          style={{ width: '100%' }}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          onEndReached={this.loadMoreAlbums}
          onEndReachedThreshold={0.4}
          ItemSeparatorComponent={() => <View style={{ height: 40 }} />}
          data={this.data}
          renderItem={item => {
            item = item.item;
            let artistNames = "";
            item.artists.forEach((artist, index) => {
              artistNames += artist.name;
              if (item.artists[index + 1]) {
                artistNames += ", ";
              }
            });
            console.log(item);
            return (
              <ArtistCard
                total_tracks={item.total_tracks}
                image={
                  //Somtime artists on spotify dose not have images
                  item.images[1] === undefined
                    ? this.noImageUrl
                    : item.images[1].url
                }
                title={item.name}
                item={item}
                type={'artistAlbums'}
                artistsNames={artistNames}
                release_date={item.release_date}
                // albumUrl={item.external_urls.spotify}
                // onClickPreview={this.onClickPreview}
                previewButton={
                  <PreviewOnButton
                    callback={this.onClickPreview}
                    text={'Preview on Spotify'}
                    albumUrl={item.external_urls.spotify}
                  />
                }
              />
            );
          }}
        />
      </View>
    );
  }
}
