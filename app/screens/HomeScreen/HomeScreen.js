import React, { Fragment } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  NativeModules,
  StatusBar,
  SafeAreaView
} from 'react-native';
import {
  createStackNavigator,
  NavigationActions,
  StackActions,
  createAppContainer,
} from 'react-navigation';
import LoginButton from './LoginButton/LogingButton';
import styles from './styles';
import { SearchScreen } from '../SearchScreen/SearchScreen';
import { ArtistScreen } from '../ArtistScreen/ArttistScreen';

class HomeScreen extends React.Component {
  async componentDidMount() {
    const token = await AsyncStorage.getItem('token', null);
    if (token) {
      this.login();
    }
  }

  static navigationOptions = {
    title: 'Login',
    headerStyle: {
      backgroundColor: '#000',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };



  login = async () => {
    const result = await NativeModules.SpotifySSO.authenticate();

    if (result.token) {
      const actionToDispatch = StackActions.replace({
        routeName: 'Search',
        params: { token: result.token },
      });

      this.props.navigation.dispatch(actionToDispatch);

      AsyncStorage.setItem('token', result.token);
    }
  };

  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.topContainer}>
            <Text style={styles.TxtWelcome}>Welcome</Text>
            <Text style={styles.TxtInstruction}>click the login
             button to enter the app</Text>
          </View>
          <View style={styles.bottomContainer}>
            <LoginButton
              style={styles.loginButton}
              text="Login With Spotify"
              login={this.login}
            />
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Search: {
    screen: SearchScreen
  },
  Artist: {
    screen: ArtistScreen
  }
});

export default createAppContainer(AppNavigator);
