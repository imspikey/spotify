import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './style';
const LoginButton = props => {
  const { text, login, style } = props;

  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={() => {
        login();
      }}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default LoginButton;
