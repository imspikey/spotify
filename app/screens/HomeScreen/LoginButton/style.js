import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: '94%',
    height: '17%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: 'white',
    fontWeight: '900',
    fontSize: 19
  }
});

export default styles;
