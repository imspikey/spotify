import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  loginButton: {
    backgroundColor: 'black',
  },
  TxtInstruction:{
    fontSize:13
  },
  topContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent:'center'
  },
  bottomContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent:'flex-start'
  },
  safeArea: { flex: 1 },
  TxtWelcome: { color: 'black', fontSize: 50 }
});

export default styles;
