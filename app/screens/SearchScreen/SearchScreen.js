import React from 'react';
import { Text, TextInput, View, FlatList, UIManager } from 'react-native';
import { ArtistCard } from '../../components/ArtistCard/ArtistCard';
import { SpotifyApi } from '../../lib/api/SpotifyApi';
import styles from './styles';

export class SearchScreen extends React.Component {
  token = '';
  offset = 0;
  previousQuery = "";
  noImageUrl =
    "https://wingslax.com/wp-content/uploads/2017/12/no-image-available.png";

  constructor(props) {
    super(props);
    this.state = { text: '', data: {}, refreshing: false };
    this.token = this.props.navigation.getParam('token');
  }

  static navigationOptions = {
    title: 'Artists',
    headerStyle: {
      backgroundColor: 'rgb(39,40,42)'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    },
  };

  searchApi = query => {
    this.setState({ refreshing: true });

    SpotifyApi.getArtists(query, 10, this.offset, this.token, (res, err) => {
      if (err) {
        alert(err.message);
        this.setState({ refreshing: false });
        return;
      }
      this.data = this.offset > 0 ? this.data.concat(res.items) : res.items;
      this.setState({ refreshing: false });
    });
  };

  onSearchChanged = text => {
    text = text.trim();
    if (text.length > 2) {
      {
        //dont call search api if the query text has not been changed.
        if (this.previousQuery === text) {
          return;
        }

        this.previousQuery = text;
        this.offset = 0;
        this.data = [];
        this.searchApi(text);
      }
    }
  };

  handleRefresh = () => {
    this.offset = 0;
    this.data = [];
    this.searchApi(this.previousQuery);
    this.cancelLoadMoreArtists = true;
  };

  loadMoreArtists = () => {
    if (this.cancelLoadMoreArtists) {
      this.cancelLoadMoreArtists = false;
      return;
    }
    this.offset += 10;
    this.searchApi(this.previousQuery);
  };

  showArtistData = id => {
    this.props.navigation.navigate('Artist', {
      artistId: id,
      token: this.token,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
            placeholderTextColor={'#fff'}
          style={styles.search}
          onChangeText={text => {
            this.setState({ text });
            this.onSearchChanged(text);
          }}
          value={this.state.text}
          placeholder={"Search for an artist..."}
        />
        <FlatList
          contentContainerStyle={{ alignItems: 'center' }}
          style={{ width: '100%',marginTop:20 }}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          onEndReached={this.loadMoreArtists}
          onEndReachedThreshold={0.4}
          data={this.data}
          renderItem={item => {
            item = item.item;
            return (
              <ArtistCard
                image={
                  //Somtime artists on spotify dose not have images
                  item.images[1] === undefined
                    ? this.noImageUrl
                    : item.images[1].url
                }
                title={item.name}
                item={item}
                followers={item.followers.total}
                rating={item.popularity / 20}
                type={'artistSearch'}
                onPress={this.showArtistData}
                id={item.id}
              />
            );
          }}
        />
      </View>
    );
  }
}
