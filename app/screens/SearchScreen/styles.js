import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: { backgroundColor:`rgb(18,18,18)`,flex: 1, alignItems: 'center', justifyContent: 'center' },
  searchInput: {
    backgroundColor: 'orange'
  },
  search: {
    backgroundColor: 'rgb(39,40,42)',
    height: 40,
    marginTop: 5,
    width:"94%",
    borderRadius:3,
    color:'white',
    textAlign:"center"
  }
});

export default styles;
